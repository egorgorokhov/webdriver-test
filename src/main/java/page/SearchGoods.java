package page;

import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchGoods {

    private final WebDriver driver;

    @FindBy(name = "query")
    private WebElement searchBar;
    @FindBy(css = "svg.t-store__search-icon.js-store-filter-search-btn")
    private WebElement searchButton;
    @FindBy(css = "span[class=\"js-store-filters-prodsnumber\"]")
    private WebElement goodsNumber;
    @FindBy(xpath = "//div[contains(@class, \"t-store__card__title\")]")
    private WebElement goodName;
    @FindBy(xpath = "//div[contains(@class,\"js-product-price\")]")
    private WebElement goodPrice;


    public SearchGoods(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public SearchGoods enterquery(String query){
        searchBar.sendKeys(query);
        return this;
    }

    public SearchGoods clickSearch(){
        searchButton.click();
        return this;
    }

    public void goodsNumberShouldBe(SoftAssertions softAssert, String number){
        softAssert.assertThat(goodsNumber.getAttribute("innerText")).as("Неправильное количество результатов поиска")
                .isEqualToIgnoringCase(number);
    }

    public void goodsNameShouldBe(SoftAssertions softAssert, String name){
        softAssert.assertThat(goodName.getText()).as("Неправильное название товара")
                .isEqualToIgnoringCase(name);
    }

    public void goodsPriceShouldBe(SoftAssertions softAssert, String price){
        softAssert.assertThat(goodPrice.getText()).as("Неправильная цена")
                .isEqualToIgnoringCase(price);
    }


}
