package page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

public class HomePage {
    private final WebDriver driver;
    private final String baseURL = "https://homebrandofficial.ru/wear";

    public HomePage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public void openHomePage(){
        driver.get(baseURL);
    }

    public void loadWait(){
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3L));
    }
}
