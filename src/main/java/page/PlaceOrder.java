package page;

import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class PlaceOrder {
    private final WebDriver driver;

    @FindBy(css = "a[href=\"https://homebrandofficial.ru/popular/tproduct/561535806-930803998551-futbolka-oversize\"]")
    private WebElement goodLink;
    @FindBy(css = "a.t-store__prod-popup__btn.t-btn.t-btn_sm")
    private WebElement addToCartBtn;
    @FindBy(css = "div.t706__carticon.t706__carticon_sm.t706__carticon_showed")
    private WebElement cartBtn;
    @FindBy(css = "button.t706__sidebar-continue.t-btn")
    private WebElement placeOrderBtn;
    @FindBy(id = "input_1496239431201")
    private WebElement nameInput;
    @FindBy(css = "input[style=\"color: rgb(94, 94, 94);\"]")
    private WebElement phoneInput;
    @FindBy(id = "input_1627385047591")
    private WebElement regionInput;
    @FindBy(id = "input_1630305196291")
    private WebElement addressInput;
    @FindBy(css = "input.searchbox-input.js-tilda-rule.t-input")
    private WebElement deliveryCity;
    @FindBy(css = "div[data-name=\"Томск\"]")
    private WebElement dropdownCity;
    @FindBy(css = "input[value=\"DHL (Доставка в другие страны)\"]")
    private WebElement dhlCheckbox;
    @FindBy(css = "input[placeholder=\"Промокод\"]")
    private WebElement promocode;
    @FindBy(xpath = "//button[@class = \"t-submit\" and text() = \"ОФОРМИТЬ ЗАКАЗ\"]")
    private WebElement submitButton;
    @FindBy(css = "div#error_1496239478607")
    private WebElement phoneInputError;
    @FindBy(xpath = "//p[contains(text(), \"корректный номер телефона\")]")
    private WebElement bottomPhoneError;

    public PlaceOrder(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public PlaceOrder goToGoodPage(){
        goodLink.click();
        return this;
    }

    public PlaceOrder addToCart(){
        addToCartBtn.click();
        return this;
    }

    public PlaceOrder goToCart(){
        cartBtn.click();
        return this;
    }

    public PlaceOrder checkoutProceed(){
        placeOrderBtn.click();
        return this;
    }

    public PlaceOrder formFill(String name, String phone, String region, String address, String city, String code){
        nameInput.sendKeys(name);
        phoneInput.sendKeys(phone);
        regionInput.sendKeys(region);
        addressInput.sendKeys(address);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click();", dhlCheckbox);
        deliveryCity.clear();
        deliveryCity.sendKeys(city);
        dropdownCity.click();
        promocode.sendKeys(code);
        return this;
    }

    public PlaceOrder submit(){
        submitButton.click();
        return this;
    }

    public boolean elementIsPresent(WebElement element){
        return element != null;
    }

    public void phoneInputErrorIsPresent(SoftAssertions softAssert){
        softAssert.assertThat(elementIsPresent(phoneInputError)).as("Элемент около поля \"Телефон\" не найден")
                .isEqualTo(true);
    }

    public void pageBottomErrorIsPresent(SoftAssertions softAssert){
        softAssert.assertThat(elementIsPresent(bottomPhoneError)).as("Элемент внизу страницы не найден")
                .isEqualTo(true);
    }

}
