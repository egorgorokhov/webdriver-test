package HomeBrandOfficialSiteTest;

import org.assertj.core.api.SoftAssertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import page.HomePage;
import page.PlaceOrder;
import page.SearchGoods;



public class WebDriverTest {
    protected WebDriver driver;
    private HomePage homePage;
    private SearchGoods searchGoods;
    private PlaceOrder placeOrder;

    @Before
    public void setup(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        searchGoods = new SearchGoods(driver);
        homePage = new HomePage(driver);
        placeOrder = new PlaceOrder(driver);
    }

    @After
    public void сloseWindow(){
        driver.quit();
    }


    @Test
    public void TestFirstScenario()  {
        SoftAssertions softAssertions = new SoftAssertions();

        homePage.openHomePage();

        homePage.loadWait();

        searchGoods.enterquery("Лонгслив White&Green")
                .clickSearch();

        String ExpectedGoodsNumber = "1";
        searchGoods.goodsNumberShouldBe(softAssertions, ExpectedGoodsNumber);

        String ExpectedName = "Лонгслив White&Green";
        searchGoods.goodsNameShouldBe(softAssertions, ExpectedName);

        String ExpectedPrice = "2 800";
        searchGoods.goodsPriceShouldBe(softAssertions, ExpectedPrice);

        softAssertions.assertAll();
    }

    @Test
    public void TestSecondScenario(){
        SoftAssertions softAssert = new SoftAssertions();

        homePage.loadWait();

        homePage.openHomePage();

        placeOrder.goToGoodPage();

        homePage.loadWait();

        placeOrder.addToCart();

        homePage.loadWait();

        placeOrder.goToCart();

        placeOrder.checkoutProceed();

        homePage.loadWait();

        placeOrder.formFill("Иванов Петр Васильевич", "0000000000", "Томская область",
                "город Томск, улица Герцена, дом 12, квартира 54","Томск","faij21h41");

        placeOrder.submit();

        placeOrder.phoneInputErrorIsPresent(softAssert);

        placeOrder.pageBottomErrorIsPresent(softAssert);

        softAssert.assertAll();

    }
}
